# Examples of Domotz API usage in PHP

## Dependencies

- PHP 7+
- composer

  

## Running

### Report, as cvs file, all the device status changes in a given period for an agent

```
composer install
php devices_history.php [agent_id] [from] [to] [base_url] [api Key]
```

